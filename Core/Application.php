<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 19:20
 */

class Application
{
    public static $classMap=[];
    public static function run(){
        self::load();
        spl_autoload_register([__CLASS__,'autoload']);
    }
    public static function load(){
        $autoPath=__DIR__.'/Smarty/Autoloader.php';
        $bootPath=__DIR__.'/Smarty/bootstrap.php';
        include($autoPath);
        include ($bootPath);
    }
    public static function autoload($class){
        $temp=realpath('./').'/'.$class;
        $filePath=str_replace('\\','/',$temp).'.php';
        if(isset(self::$classMap[$class]))
            return true;
        else{
            if(is_file($filePath)){
                include $filePath;
                self::$classMap[$class]=$class;
            }
        }

    }
}