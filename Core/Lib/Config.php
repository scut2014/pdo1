<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 19:24
 */

namespace Core\Lib;


class Config
{
    protected static $config=[];
    static function  load($path='/app/config.php'){
        $path=realpath('./').$path;
        $path=str_replace('\\','/',$path);
        self::$config=include ($path);
    }
    //database.type=mysql
    //Config::get('database.type')
    static function get($key){
        if(!self::$config)
            self::load();
        return self::array_get($key,self::$config);

    }

    static function set($key,$value){
        if(!self::$config)
            self::load();
        return self::array_set($key,$value,self::$config);
    }
//database.master.type

    private  static  function array_get($str,$array){
        $list=explode('.',$str);//[database,master,type]
        $first=array_shift($list);//[master,type]
        $result=isset($array[$first])?$array[$first]:null;// [        'type'=>'pdo',            'dbName'=>'test',        ]
        if($result&&is_array($result)&&count($list)>0){
            return self::array_get(join('.',$list),$result);
        }
        return $result;
    }

    private static function array_set($str,$val,&$array){
        $list=explode('.',$str);
        $first=array_shift($list);
        if(count($list)>0)
            return self::array_set(join('.',$list),$val,$array[$first]);
        $array[$first]=$val;
    }
}