<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 19:42
 */

namespace Core\Lib;


class Helper
{
    static function unlimited($arr,$pid=0,$level=0){
        static  $result=[];
        foreach ($arr as $v){
            if($v->pid==$pid)
            {
                $v->html=str_repeat('&nbsp;',$level*3);
                $result[]=$v;
                self::unlimited($arr,$v->id,$level+1);
            }

        }
        return $result;
    }

    // 根据节点pid找出所有子节点，返回多维数组，按照一级节点一个child层级
    static function unlimitedForLayer($cate,$pid=0){
        $arr=[];
        foreach ($cate as $c){
            if($c->pid==$pid){
                $c->child=self::unlimitedForLayer($cate,$c->id);
                $arr[]=$c;
            }
        }
        return $arr;
    }

    //根据 节点id找出所有父节点
    static function parent($cate,$id=0){
        $arr=[];
        foreach ($cate as $c){
            if($c->id==$id){
                $arr[]=$c;
                $arr=array_merge($arr,self::parent($cate,$c->pid));
            }
        }
        return $arr;
    }
}