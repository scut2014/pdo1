<?php
namespace Core\Lib;
class DB{
    private $config=[];
    protected  $db=null;
    private $pk='id';
    private $table='';
    private $selectSql='';
    private $whereSql='';
    private $havingSql='';
    private $groupSql='';
    private $orderSql='';
    private $limitSql='';
    private $joinSql='';
    private $params=[];
    private $values=[];
    public function __construct($config=[]){
        $this->config=\Core\Lib\Config::get('database');
        $this->config=array_merge($this->config,$config);
        if($this->db==null)
            $this->db=$this->connect();
    }
    private function connect(){
            try{
        //第一种连接mysql的方式
        $dsn="mysql:host={$this->config['host']};dbname={$this->config['dbname']};charset={$this->config['charset']}";
        $username=$this->config['username'];
        $password=$this->config['password'];
        $pdo=new \PDO($dsn,$username,$password);
        return $pdo;
        }catch(\PDOException $e){
            echo "错误信息提示<br>";
            echo $e->getMessage();
            return null;
        } 
    }
    //可以执行  select insert update delete 
    public function query($sql){
        dump($sql);
       $stmt=$this->db->query($sql);
       if(!$stmt)
            print_r($this->db->errorInfo());
        return $stmt->fetchAll(\PDO::FETCH_CLASS);
    }
   //只能insert update delete 语句
    public function exec($sql){
       $res=$this->db->exec($sql);
       echo $this->db->lastInsertId();
       return $res;
    }
    function all(){
        $sql=$this->selectSql.$this->whereSql.$this->joinSql.$this->groupSql.$this->havingSql.$this->orderSql.$this->limitSql;
        $stmt=$this->run($sql);
        $res=$stmt->fetchAll(\PDO::FETCH_CLASS);
        $this->reset();
         return $res;
    }
    function reset(){
        $this->selectSql='';
        $this->whereSql='';
        $this->groupSql='';
        $this->havingSql='';
        $this->orderSql='';
        $this->limitSql='';
        $this->params=[];
        $this->values=[];
        $this->joinSql='';
    }

    function select($fields=[]){
        //$fields为数组的情况
        if(is_array($fields)){
            array_walk($fields,[$this,'parseField']);
            $str=implode(',',$fields);
        }
        else{
          $str=$this->table.'.'.$fields;
        }
        $this->selectSql='SELECT '.$str.' FROM '.$this->table;
        return $this;
    }

    function where($field,$condition='=',$value='',$flag='and'){
        $number=func_num_args();
        if($number==1)
        {
            $param=func_get_arg(0);
            if(!is_array($param))
                dd('参数错误');
            //['id','=',1]
            else{
                //是一维数组
                if($this->dimension($param)){
                   $this->where($param[0],$param[1],$param[2],$flag);
                }else{//是二维数组
                   foreach ($param as $p){
                       $this->where($p);
                   }
                }
            }
        }else{
            $this->params[]=':'.$field;
            $params[$field]=':'.$field;
            $this->values[]=$value;
            $str='';
            foreach ($params as $k=>$v){
                $str.=' '.$this->parseField($k).$condition.$v;
            }
            if(empty($this->whereSql)){
                $this->whereSql=' WHERE '.$str;
            }else{
                $flag=' '.strtoupper($flag).' ';
                $this->whereSql.=$flag.$str;
            }
        }

        return $this;
    }
    function orWhere($field,$condition='=',$value=''){
        return $this->where($field,$condition,$value,'or');
    }
    function dimension($arr){
        if(count($arr)==count($arr,COUNT_RECURSIVE)){
            return 1;
        }
        else{
            return 0;
        }
    }
    function parseValue($v){
        if(!is_numeric($v)){
            return "'{$v}'";
        }else{
            return $v;
        }
    }
    function parseField(&$v){
        $pattern='/\./';
        if(!preg_match($pattern,$v)){
            $v=$this->table.'.'.$v ;
        }
        return $v;

    }
    function table($table){
        $this->table="`{$table}`";
        return $this;
    }

    function insert2($arr){
        if($this->dimension($arr)){
            $arr=[$arr];
            $this->insert($arr);
        }else{
            $keys=array_keys($arr[0]);// [username,password,email]
            $this->params=$keys;
            array_walk($this->params,function(&$v){
                $v=':'.$v;
            });
            $paramsStr=implode(',',$this->params);
            $keysStr=implode(',',$keys);

            dump($this->params);
            array_walk($keys,function(&$v){
                $v='`'.$v.'`';
            });
           foreach ($arr as $v){
               $this->values[]=array_values($v);
           }
          dump($this->values);

            $sql='INSERT INTO '.$this->table.'('.$keysStr.') VALUES('.$paramsStr.')';

            $stmt=$this->db->prepare($sql);
            $len=count($this->params);
            foreach ($this->values as $v){
                for($i=0;$i<$len;$i++){
                    $stmt->bindValue($this->params[$i],$v[$i]);
                }
                $stmt->execute();
            }
            if(DEBUG){
                echo "<pre>";
                $stmt->debugDumpParams();
            }
        }
    }

    function insert($arr){
        if($this->dimension($arr)){
            $field=array_keys($arr);
            $fieldStr=implode(',',$field);
           $this->params=array_keys($arr);
           $this->values=array_values($arr);
           array_walk($this->params,function(&$v){
               $v=':'.$v;
            });
            $paramsStr=implode(',',$this->params);
          $sql='INSERT INTO '.$this->table.'('.$fieldStr.')'.' VALUES ('.$paramsStr.')';
          $this->run($sql);
        }else{
            foreach ($arr as $v){
                $this->insert($v);
            }
        }
    }
//['username'=>'king001','email'=>'king001@163.com'] ['id'=>1]
// update user set user.username=:username,user.email=:email where id=:id
    function update($arr){
             $keys=array_keys($arr);
             $values=array_values($arr);
             $t=$keys;
             array_walk($t,function(&$v){
                $this->params[]=':'.$v;
                 $v=$this->table.'.'.$v.'=:'.$v;
             });
             array_walk($values,function(&$v){
                 $this->values[]=$v;
             });
            $sql='UPDATE '.$this->table.' SET '.implode(',',$t).$this->whereSql;
           $this->run($sql);
    }
    function delete($id=''){
        if(!empty($id)){
            $this->whereSql=' WHERE '.$this->pk.'='.$id;
        }
        $sql='DELETE FROM '.$this->table.$this->whereSql;
       $this->run($sql);
    }
    
    function run($sql){
        $stmt=$this->db->prepare($sql);
        $len=count($this->params);
        for ($i=0;$i<$len;$i++){
            $stmt->bindValue($this->params[$i],$this->values[$i]);
        }
        $stmt->execute();
        if(DEBUG){
            echo "<pre>";
            $stmt->debugDumpParams();
        }
        $this->reset();
        return $stmt;
    }
    public function transaction(){
        // var_dump($this->db->inTransaction());//false
        $this->db->beginTransaction();
        $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,0);
        // var_dump($this->db->inTransaction());//true
        try{
            $number=500;
             $sql="update test set money=money-{$number} where id=1";
             $moneySql='select money from test where id=1';
             $res=$this->query($moneySql)[0];
             $money=$res->money;
             if($money<$number)
                throw new PDOException('金额不足');
             else{
                 if($this->db->exec($sql)){
                    $sql2="update test set money=money+{$number} where id=4";
                    if($this->db->exec($sql2)){
                        $this->db->commit();//提交
                    }else{
                        throw new PDOException('赵敏收款失败');
                    }
                 }else{
                     throw new PDOException('张无忌转账失败');
                 }
                
             }
        }catch(PDOException $e){
                $this->db->rollback();//回滚
                echo $e->getMessage();
        }
    }

// category,id,=,cate_id
    function join($joinTable,$joinField,$condition,$field,$type='left'){
        $type=' '.strtoupper($type).' ';
          $this->joinSql=$type.' JOIN `'.$joinTable.'`'.' ON `'.$joinTable.'`.'.$joinField.$condition.$this->table.'.'.$field;
          return $this;
        }
    function leftJoin($joinTable,$joinField,$condition,$field){
        return $this->join($joinTable,$joinField,$condition,$field,'left');
    }
    function innerJoin($joinTable,$joinField,$condition,$field){
        return $this->join($joinTable,$joinField,$condition,$field,'inner');
    }

    function rightJoin($joinTable,$joinField,$condition,$field){
        return $this->join($joinTable,$joinField,$condition,$field,'right');
    }
    

}