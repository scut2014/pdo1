<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 20:33
 */

namespace Core\Lib;


class View
{
   private static $smarty=null;
   private function  __construct()
   {

   }
   private function __clone()
   {

   }

    public static function Smarty(){
       if(self::$smarty==null)
           self::$smarty=new \Smarty();
       $config=Config::get('smarty');
       self::$smarty->setTemplateDir($config['templateDir']);
       self::$smarty->setCompileDir($config['compileDir']);
       self::$smarty->setLeftDelimiter($config['leftDelimiter']);
       self::$smarty->setRightDelimiter($config['rightDelimiter']);
       return self::$smarty;
   }
}