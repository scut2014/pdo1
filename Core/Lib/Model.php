<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 19:31
 */

namespace Core\Lib;


class Model extends DB
{

    function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->tableName();
    }

    function tableName(){
        $namespace=get_class($this);
        $pos=strrpos($namespace,'\\');
        $table=strtolower(substr($namespace,$pos+1));
        $this->table($table);
    }
}