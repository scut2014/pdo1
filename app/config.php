<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 19:23
 */
return [
    'database'=>[
        'host'=>'localhost',
        'username'=>'root',
        'password'=>'root',
        'dbname'=>'blog',
        'charset'=>'utf8'
    ],
    'smarty'=>[
        'leftDelimiter'=>'{',
        'rightDelimiter'=>'}',
        'templateDir'=>ROOT.'/app/View/template',
        'compileDir'=>ROOT.'/app/View/compile'
    ]
];