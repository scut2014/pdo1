<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/20
 * Time: 20:17
 */

namespace App\Http\Controller;


use App\Http\Model\User;
use Core\Lib\Controller;

class UserController extends Controller
{
    function index(){
         $user=new User();
         $users=$user->users();
         self::$view->assign(compact('users'));
         self::$view->display('index.html');
     }
}