 <?php
header("content-type:text/html;charset=utf-8");
include 'Core/Application.php';

 define('DEBUG',0);
 define('ROOT',__DIR__);
 define('DOMAIN','http://course.hd/pdo1');
 Application::run();
require 'vendor/autoload.php';
//实例化服务器容器，注册事件，路由服务提供者
$app = new Illuminate\Container\Container;  //服务容器【服务的注册和解析】
with(new Illuminate\Events\EventServiceProvider($app))->register();
with(new Illuminate\Routing\RoutingServiceProvider($app))->register();
require 'app/routes.php';
 //实例化请求并分发处理请求
 $request = Illuminate\Http\Request::CreateFromGlobals();
 $response = $app['router']->dispatch($request);
 //返回请求响应
 $response->send();


//[
 //   ['id','=',1],
  //  ['username','!=','king2']
//  //]
//$res=$db->table('category')->select(['id','pid','name'])->all();
//dd($res);
// $res2=$db->table('user')->select('username')->where('id','=',1)->all();
// $arr=[

//  [ 'username'=>'king1005'  ,
//     'password'=>md5(123456),
//     'email'=>'king1005@qq.com'],
//     [ 'username'=>'king1004'  ,
//         'password'=>md5(123456),
//         'email'=>'king1004@qq.com'],
// ];
// $db->table('user')->insert($arr);
//$db->table('user')->where('id','=',1)->update(['username'=>'king001','email'=>'king001@163.com']);
//$db->table('user')->where('id','>',38)->delete();
//$db->transaction();
// 

//  $res=$db->query($sql);
// $db->prepare();
// echo "<pre>";
// foreach($res as $r){
//     echo "ID:{$r->id}<br>";
//     echo "username:{$r->username}<br>";
//     echo "email:{$r->email}<br>";
//     echo "admin:{$r->is_admin}<br>";
//     echo "<hr>";
// }
// $sql2="insert into user(username,password,email) values('guest4',md5(123456),'guest4@qq.com'),('guest5',md5(123456),'guest5@qq.com')";
// var_dump($db->exec($sql2));
// try{
//     //第2种连接mysql的方式
//     $dsn='uri:file://D:\phpStudy\PHPTutorial\WWW\pdo1\dsn.txt';
//     $username='root';
//     $password='root';
//     $pdo=new PDO($dsn,$username,$password);
//     var_dump($pdo);
// }catch(PDOException $e){
//     echo $e->getMessage();
// }
// try{
//     //第3种连接mysql的方式 在php.ini中写入 pdo.dsn.cj="mysql:host:localhost;dbname=blog;charset=utf8";
//     $dsn='cj';
//     $username='root';
//     $password='root';
//     $pdo=new PDO($dsn,$username,$password);
//     var_dump($pdo);
// }catch(PDOException $e){
//     echo $e->getMessage();
// }
